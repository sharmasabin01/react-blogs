import "./single.css";
import Singlepost from "../../components/singlePost/Singlepost";

export default function Single() {
  return (
    <div className="single">
        <Singlepost />
    </div>
  );
}
