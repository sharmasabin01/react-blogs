 
import { useEffect, useState } from "react"
import Header from "../../components/header/Header"
import Posts from "../../components/posts/Posts"
import "./home.css"
import axios from "axios"
import { useLocation } from "react-router"

export default function Home() {
  const [posts, setposts] = useState([]);
  const { search } = useLocation();

  useEffect(()=>{
    const fetchPosts = async ()=> {
      const res = await axios.get("/posts" + search);
      setposts(res.data)
    };

    fetchPosts();
  }, [search]);
  return (
    <>

    <Header/>

    <div className="home">
        <Posts posts={posts} />
    
    </div>
  </>
  );
}
