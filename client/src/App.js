import Home from "./pages/home/Home";
import TopBar from "./components/topbar/TopBar";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import Setting from "./pages/settings/Setting";
import Single from "./pages/single/Single";
import Write from "./pages/write/Write";
import { useContext } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import { Context } from "./context/Context";


function App() {
  const { user } = useContext(Context);
  return (
    <div>
    <Router>
    <TopBar/>
    <Routes>
      <Route exact path='/' element={<Home/>}></Route>
      <Route  path='/Register' element={user ? <Home/> :<Register />}></Route>
      <Route  path="/Login" element={user ? <Home/> :<Login />} ></Route>
      <Route  path="/Write" element={user ? <Write /> : <Login />}> </Route>
      <Route  path="/settings" element={user ? <Setting/> :<Login />}></Route>
      <Route  path="/post/:postId" element={<Single/>} ></Route>
    </Routes>
    </Router>
    </div>
 
  );
}

export default App;
