This is a personal blog app.

Used technologies:
React js (front end)
Node js (back end)


Features:

* Homepage displays all the blog posts from every user.
* User should register and login to add a blog.
* User can add blogs from write page.
* User can add picture, title and description about his blog through write page.
* User can see blog posts created by every users.
* User can only edit and delete blog posts created by himself/herself.
* User can redirect to setting page clicking the profile icon.
* User can update his/her account details including profile picture, username, email and passwords.
* User can logout through his account and still can see blogs posted in homepage. 
